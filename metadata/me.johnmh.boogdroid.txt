Categories:Development
License:GPLv3
Web Site:
Source Code:https://github.com/JohnMHarrisJr/BoogDroid
Issue Tracker:https://github.com/JohnMHarrisJr/BoogDroid/issues

Auto Name:BoogDroid
Summary:Bugzilla client
Description:
Browse bugzilla bug tracker.
.

Repo Type:git
Repo:https://github.com/JohnMHarrisJr/BoogDroid

Build:0.0.1,13
    commit=81eb5d60eb19c1849242ec9afaac20e30f26179a
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.0.1
Current Version Code:13
